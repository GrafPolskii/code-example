from flask_restful import Resource, fields, reqparse
from flask_restful_swagger import swagger

from app.models import Movies

from app.utils.search import search
from app.utils.sort import sort

@swagger.model
class MovieItem:
    def __init__(self, imdbid, title, year, rated, released, runtime, genre, director, 
        writer, actors, plot, language, country, awards, poster, rating, metascore, 
        imdbrating, imdbvotes, type_, dvd, boxoffice, production, website):
        pass

    resource_fields = {
        'imdbid': fields.String(attribute='imdbid'),
        'title': fields.String(attribute='title'), 
        'year': fields.String(attribute='year'), 
        'rated': fields.String(attribute='rated'), 
        'released': fields.String(attribute='released'), 
        'runtime': fields.String(attribute='runtime'), 
        'genre': fields.List(fields.String, attribute='genre'), 
        'director': fields.String(attribute='director'), 
        'writer': fields.String(attribute='writer'), 
        'actors': fields.String(attribute='actors'), 
        'plot': fields.String(attribute='plot'), 
        'language': fields.String(attribute='language'), 
        'country': fields.List(fields.String, attribute='country'), 
        'awards': fields.String(attribute='awards'), 
        'poster': fields.String(attribute='poster'), #URL?
        'rating': fields.String(attribute='rating'), 
        'metascore': fields.String(attribute='metascore'), 
        'imdbrating': fields.String(attribute='imdbrating'), 
        'imdbvotes': fields.String(attribute='imdbvotes'), 
        'type': fields.String(attribute='type_'), 
        'dvd': fields.String(attribute='dvd'), 
        'boxoffice': fields.String(attribute='boxoffice'), 
        'production': fields.String(attribute='production'), 
        'website': fields.String(attribute='website')
    }


@swagger.model
@swagger.nested(movies=MovieItem.__name__)
class ResponseAllMovies:
    def __init__(self, movies, status, error):
        pass

    resource_fields = {
            'movies': fields.List(fields.Nested(MovieItem.resource_fields), attribute="movies"),
            'code': fields.Integer(attribute='code', default=200),
            'error': fields.Boolean(attribute='error', default=False)
        }


@swagger.model
@swagger.nested(movie=MovieItem.__name__)
class ResponseIDMoviesSuccess:
    def __init__(self, movie, status, error):
        pass

    resource_fields = {
            'movie': fields.Nested(MovieItem.resource_fields),
            'code': fields.Integer(attribute='code'),
            'error': fields.Boolean(attribute='error')
        }


@swagger.model
class ResponseIDMoviesFail:
    def __init__(self, error_msg, status, error):
        pass
    resource_fields = {
            'msg': fields.String(attribute='msg', default="Not found"),
            'code': fields.Integer(attribute='code'),
            'error': fields.Boolean(attribute='error')
    }


@swagger.model
class SortingFields:
    def __init__(self, title, genre, year, country):
        pass
    resource_fields = {
            'title': fields.String(attribute='title', default='none'),
            'genre': fields.String(attribute='genre', default='none'),
            'year': fields.String(attribute='year', default='none'),
            'country': fields.String(attribute='country', default='none')
    }


@swagger.model
@swagger.nested(sorting=SortingFields.__name__)
class Sorting:
    def __init__(self, sorting):
        pass
    resource_fields = {
            'sorting': fields.Nested(SortingFields.resource_fields)
    }


class AllMovies(Resource):
    @swagger.operation(
        notes='Get list of all movies',
        nickname='getmovies',
        responseClass=ResponseAllMovies,
        responseMessages=[
            {
                "message": "Successful operation",
                "code": 200
            }
        ])
    def get(self):
        movies_list = []
        movies = Movies.query.all()
        for movie in movies:
            movies_list.append(movie.__repr__())
        return {'movies': movies_list, 'code': 200, 'error': False}


class IDMovies(Resource):
    @swagger.operation(
        notes='Get info about a movie by id',
        nickname="getmovie",
        responseClass=ResponseIDMoviesSuccess,
        parameters=[
            {
                "name": "movie_id",
                "description": "ID of a movie to return",
                "required": True,
                "allowMultiple": False,
                "paramType": "path",
                "dataType": "string"
            }],
        responseMessages=[
            {
                "message": "Successful operation",
                "code": 200
            },
            {
                "message": "Not found movie",
                "code": 404
            }
        ])
    def get(self, movie_id):
        movie = Movies.query.filter_by(imdbid=movie_id).first()
        if movie != None:
            movie = movie.__repr__()
            return {'movie': movie, 'code': 200, 'error': False}
        else:
            return {'msg': "Not found", 'code': 404, 'error': True}, 404


class Search(Resource):
    @swagger.operation(
        notes='Get filtered and sorted movies list',
        nickname='searchmovie',
        responseClass=ResponseAllMovies,
        parameters=[
            {
                "name": "title",
                "description": "Movie title to search",
                "defaultValue": "all",
                "required": False,
                "allowMultiple": False,
                "paramType": "query",
                "dataType": "string"
            },
            {
                "name": "genre",
                "description": "Movie genre to search (first letter is capital, comma-delimited args)",
                "defaultValue": "all",
                "required": False,
                "allowMultiple": False,
                "paramType": "query",
                "dataType": "string",
            },
            {
                "name": "year",
                "description": "Movie announcement year to search (comma-delimited args)",
                "defaultValue": "all",
                "required": False,
                "allowMultiple": False,
                "paramType": "query",
                "dataType": "string"
            },
            {
                "name": "country",
                "description": "Movie country to search (first letter is capital, comma-delimited args)",
                "defaultValue": "all",
                "required": False,
                "allowMultiple": False,
                "paramType": "query",
                "dataType": "string"
            },
            {
                "name": "sorting",
                "description": "JSON object with sorting filters (don't work at swagger)",
                "defaultValue": {},
                "required": False,
                "allowMultiple": False,
                "paramType": "body",
                "dataType": "Sorting"
            }],
             responseMessages=[
            {
                "message": "Successful operation",
                "code": 200
            }
        ])
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('title', type=str, default='all', case_sensitive=False, trim=True)
        parser.add_argument('genre', type=str, default='all', case_sensitive=True, trim=True)
        parser.add_argument('year', type=str, default='all', case_sensitive=True, trim=True)
        parser.add_argument('country', type=str, default='all', case_sensitive=True, trim=True)
        parser.add_argument('sorting', type=dict, default={}, case_sensitive=False, location='json')
        args = parser.parse_args()
        movies = search(args)
        movies = sort(args.sorting, movies)

        movies_list = []
        movies = movies.all()
        for movie in movies:
            movies_list.append(movie.__repr__())

        return {'movies': movies_list, 'code': 200, 'error': False}